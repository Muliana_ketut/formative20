package com.code20.code20.controller;

import java.util.ArrayList;
import java.util.List;

import com.code20.code20.model.User;
import com.code20.code20.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
// import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class UserController {
    @Autowired
    private UserRepository repoUser;

    // getAll user
    @GetMapping("/user")
    public String getAllUser(Model model) {
        List<User> listUser = repoUser.findAll();
        model.addAttribute("listUser", listUser);
        return "user";
    }

    @PostMapping(value = "/user/save", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addUser(@RequestBody ArrayList<User> user) {
        repoUser.saveAll(user);
        return "redirect:/user";
    }
}
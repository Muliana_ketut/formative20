package com.code20.code20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code20Application {

	public static void main(String[] args) {
		SpringApplication.run(Code20Application.class, args);
	}

}

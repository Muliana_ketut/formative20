//  GET request using fetch()
fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
// Converting received data to JSON
.then((response) => response.json())
.then((json) => {
    let options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify(json),
    };

    // Fake api for making post requests
    let fetchRes = fetch("http://localhost:8080/user/save", options);
});